var current = 1;
var slides_number = 0;
var print_mode = false;
var current_cursor = 'default';
var help_showed = false;
var slideshow = false;
var slideshow_interval_time = 1000;
var slideshow_interval = null;

// const

var A = 64;
var B = 65;
var C = 66;
var H = 72;
var P = 80;
var S = 83;
var Z = 90;
var LEFT_ARROW = 37;
var TOP_ARROW = 38;
var RIGHT_ARROW = 39;
var BOTTOM_ARROW = 40;
var SPACE = 82;

function hide(elem)
{
  $(elem).css('opacity', 0);
  $(elem).css('z-index', 0);
}

function show(elem)
{
  $(elem).css('opacity', 1);
  $(elem).css('z-index', 1000);
}

function hide_help()
{
  $('.kooki-slides-help').css('opacity', 0);
  $('.kooki-slides-help').css('z-index', 0);
}

function show_help()
{
  $('.kooki-slides-help').css('opacity', '0.8');
  $('.kooki-slides-help').css('z-index', 10000);
}

function main()
{
  var hash = window.location.hash;

  $('.slide').each(function(index)
  {
    hide(this);
    slides_number += 1;
  });

  if (hash === '#/print')
  {
    print_mode = true;
    switchToPrint();
  }

  if (!print_mode)
  {
    if (hash !== '')
    {
      new_hash = hash.replace('#/', '');
      show('.slide-' + new_hash);
      current = parseInt(new_hash);
    }
    else
    {
      show('.slide-1');
    }
  }

  // zoom();
}

function handleNewHash()
{
    var hash = window.location.hash;

    if (!print_mode)
    {
      $('.slide').each(function(index)
      {
        hide(this);
      });

      new_hash = hash.replace('#/', '');
      show('.slide-' + new_hash);
      current = parseInt(new_hash);
    }
}

function zoom()
{
    console.log("zoom");

    var section_height = $('section').outerHeight() - (0.10 * $('section').outerHeight());

    $('.slide-content').each(function(index)
    {
      $(this).css('zoom', 1);
    });

    var count = 1;

    $('.slide-content').each(function(index)
    {
      var slide_height = $(this).outerHeight();
      var ratio = 1;

      while(slide_height >= section_height)
      {
        ratio -= 0.1;
        $(this).css('zoom', ratio);
        slide_height = $(this).outerHeight() * ratio;
      }

      count+=1;
    });
};

function switchToPresentation()
{
  print_mode = false;

  $('.slide').each(function(index)
  {
    hide(this);
  });

  show('.slide-' + current);
  window.location.hash = '/' + current;

  $('body').toggleClass('print');
  $('body').toggleClass('presentation');
  $('.slides').toggleClass('slides-print');
  $('.slides').toggleClass('slides-presentation');
}

function switchToPrint()
{
  print_mode = true;

  $('.slide').each(function(index)
  {
    show(this);
  });

  window.location.hash = '/print';

  $('body').toggleClass('print');
  $('body').toggleClass('presentation');
  $('.slides').toggleClass('slides-print');
  $('.slides').toggleClass('slides-presentation');
}

$(document).ready(function()
{
  main();
});

$(document).keydown(function(e)
{
  switch(e.which)
  {
      case S:
        if ($('body').hasClass('print'))
        {
          switchToPresentation();
        }
        else
        {
          switchToPrint();
        }
        // zoom();
        break;

      case LEFT_ARROW:
        if (!print_mode && current > 1)
        {
          hide('.slide-' + current);
          current -= 1;
          $('.slide-' + current).show();
          window.location.hash = '/' + current;
        }
        break;

      case RIGHT_ARROW:
        if (!print_mode && current < slides_number)
        {
          hide('.slide-' + current);
          current += 1;
          $('.slide-' + current).show();
          window.location.hash = '/' + current;
        }
        break;

      case A:
        console.log('a');
        break;

      case B:
        console.log('b');
        break;

      case Z:
        zoom();
        break;

      case C:
        var laser = 'url(laser.svg) 20 20, auto';

        if (current_cursor == 'default')
        {
          current_cursor = laser;
          $('.presentation').css('cursor', laser);
        }
        else if (current_cursor == laser)
        {
          current_cursor = 'none';
          $('.presentation').css('cursor', 'none');
        }
        else if (current_cursor == 'none')
        {
          current_cursor = 'default';
          $('.presentation').css('cursor', 'default');
        }
        break;

      case H:
        if (help_showed)
        {
          hide_help();
        }
        else
        {
          show_help();
        }
        help_showed = !help_showed;
        break;

      case SPACE:
        if (slideshow)
        {
          clearInterval(slideshow_interval);
          $('.stop').css('display', 'block');
          setTimeout(function()
          {
            $('.stop').css('display', 'none');
          }, 1000);
        }
        else
        {
          slideshow_interval = setInterval(function()
          {
            if (!print_mode && current >= slides_number)
            {
              current = 1;
            }
            else
            {
              current += 1;
            }
            window.location.hash = "#/" + current;
          }, slideshow_interval_time);

          $('.start').css('display', 'block');
          setTimeout(function()
          {
            $('.start').css('display', 'none');
          }, 1000);
        }
        slideshow = !slideshow;
        break;

      case TOP_ARROW:
        slideshow_interval_time += 500;

        $('.time').css('display', 'block');
        $('.time').html(slideshow_interval_time + 'ms');
        setTimeout(function()
        {
          $('.time').css('display', 'none');
        }, 1000);
        break;

      case BOTTOM_ARROW:
        if (slideshow_interval_time > 500)
        {
          slideshow_interval_time -= 500;
        }

        $('.time').css('display', 'block');
        $('.time').html(slideshow_interval_time + 'ms');
        setTimeout(function()
        {
          $('.time').css('display', 'none');
        }, 1000);
        break;

      default: return; // exit this handler for other keys
  }
  e.preventDefault(); // prevent the default action (scroll / move caret)
});

$(window).resize(function()
{
  zoom()
});

$(document).load(function()
{
  // zoom()
});

// setTimeout(function() {zoom();}, 2000);

window.addEventListener('hashchange', handleNewHash, false);
